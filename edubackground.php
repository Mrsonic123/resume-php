<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet "href="eduback.css">
    <title>Document</title>
    </head>
    <body>
        <header>
            <div class="title">
                <h1>Educational Background</h1>
            </div>
                <div class="eback">
                    <table>
                        <tr>
                            <td>College: </td>
                            <td>University of Southeastern Philippines</td>
                        </tr>
                        <tr>
                            <td>Adress:</td>
                            <td>Barangay Obrero, Davao City</td>
                        </tr>
                        <tr>
                            <td>Course:</td>
                            <td>Bachelor of Science in Information Technology<br>Undergraduate</td>
                        </tr>
                        <tr>
                            <td>High School:</td>
                            <td>Assumption College of Davao</td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td>J. P. Laurel, Cabaguio Avenue, Davao City</td>
                        </tr>
                        <tr>
                            <td>School Year:</td>
                            <td>2014 – 2015</td>
                        </tr>
                        <tr>
                            <td>Elementary:</td>
                            <td>Assumption College of Davao</td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td>J.P. Laurel, Cabaguio Avenue, Davao City</td>
                        </tr>
                        <tr>
                            <td>School Year:</td>
                            <td>2010 – 2011</td>
                        </tr>
                    </table>
                </div>
        </header>
    </body>
</html>