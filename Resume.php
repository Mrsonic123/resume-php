<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="resume.css">
        <title>Personal Portfolio</title>

    </head>
    <body>   
        <header>  
                <div class="main">
                    <div class="profile">
                        <img src="me.jpg" alt="profile img">
                    </div>
                    <nav>
                        <ul>
                            <li class="active"><a href="careerobj.html">CareerObjectives</a></li> 
                            <li><a href="Skills.html">Skills</a></li>
                            <li><a href="PersonalData.html">Personal Data</a></li>
                            <li><a href="EducationalBackground.html">Educational Background</a></li> 
                        </ul>
                    </nav>
                </div>

                <div class="title">
                    <h1>Personal Portfolio</h1>
                </div>

                <div class="twitter">
                    <a href="http://www.twitter.com" target="_blank">

                            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" style="" viewBox="0 0 410.155 410.155" x="0px" y="0px" xmlns:xml="http://www.w3.org/XML/1998/namespace" xml:space="preserve" version="1.1">
                
                                <path style="fill: #76a9ea;" d="M 403.632 74.18 c -9.113 4.041 -18.573 7.229 -28.28 9.537 c 10.696 -10.164 18.738 -22.877 23.275 -37.067 l 0 0 c 1.295 -4.051 -3.105 -7.554 -6.763 -5.385 l 0 0 c -13.504 8.01 -28.05 14.019 -43.235 17.862 c -0.881 0.223 -1.79 0.336 -2.702 0.336 c -2.766 0 -5.455 -1.027 -7.57 -2.891 c -16.156 -14.239 -36.935 -22.081 -58.508 -22.081 c -9.335 0 -18.76 1.455 -28.014 4.325 c -28.672 8.893 -50.795 32.544 -57.736 61.724 c -2.604 10.945 -3.309 21.9 -2.097 32.56 c 0.139 1.225 -0.44 2.08 -0.797 2.481 c -0.627 0.703 -1.516 1.106 -2.439 1.106 c -0.103 0 -0.209 -0.005 -0.314 -0.015 c -62.762 -5.831 -119.358 -36.068 -159.363 -85.14 l 0 0 c -2.04 -2.503 -5.952 -2.196 -7.578 0.593 l 0 0 C 13.677 65.565 9.537 80.937 9.537 96.579 c 0 23.972 9.631 46.563 26.36 63.032 c -7.035 -1.668 -13.844 -4.295 -20.169 -7.808 l 0 0 c -3.06 -1.7 -6.825 0.485 -6.868 3.985 l 0 0 c -0.438 35.612 20.412 67.3 51.646 81.569 c -0.629 0.015 -1.258 0.022 -1.888 0.022 c -4.951 0 -9.964 -0.478 -14.898 -1.421 l 0 0 c -3.446 -0.658 -6.341 2.611 -5.271 5.952 l 0 0 c 10.138 31.651 37.39 54.981 70.002 60.278 c -27.066 18.169 -58.585 27.753 -91.39 27.753 l -10.227 -0.006 c -3.151 0 -5.816 2.054 -6.619 5.106 c -0.791 3.006 0.666 6.177 3.353 7.74 c 36.966 21.513 79.131 32.883 121.955 32.883 c 37.485 0 72.549 -7.439 104.219 -22.109 c 29.033 -13.449 54.689 -32.674 76.255 -57.141 c 20.09 -22.792 35.8 -49.103 46.692 -78.201 c 10.383 -27.737 15.871 -57.333 15.871 -85.589 v -1.346 c -0.001 -4.537 2.051 -8.806 5.631 -11.712 c 13.585 -11.03 25.415 -24.014 35.16 -38.591 l 0 0 C 411.924 77.126 407.866 72.302 403.632 74.18 L 403.632 74.18 Z" />
                                            
                            </svg>
                    </a>
                    <div class="fb">
                        <a class="fb" href="http://www.facebook.com" target="_blank">
                                    
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 408.788 408.788" style="enable-background:new 0 0 408.788 408.788;" xml:space="preserve">
                                            
                                <path style="fill:#475993;" d="M353.701,0H55.087C24.665,0,0.002,24.662,0.002,55.085v298.616c0,30.423,24.662,55.085,55.085,55.085  h147.275l0.251-146.078h-37.951c-4.932,0-8.935-3.988-8.954-8.92l-0.182-47.087c-0.019-4.959,3.996-8.989,8.955-8.989h37.882  v-45.498c0-52.8,32.247-81.55,79.348-81.55h38.65c4.945,0,8.955,4.009,8.955,8.955v39.704c0,4.944-4.007,8.952-8.95,8.955  l-23.719,0.011c-25.615,0-30.575,12.172-30.575,30.035v39.389h56.285c5.363,0,9.524,4.683,8.892,10.009l-5.581,47.087  c-0.534,4.506-4.355,7.901-8.892,7.901h-50.453l-0.251,146.078h87.631c30.422,0,55.084-24.662,55.084-55.084V55.085  C408.786,24.662,384.124,0,353.701,0z"/>
                                            
                            </svg>             
                        </a>
                    </div>
                        <div class="insta">
                            <a href="http://www.instagram.com" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 551.034 551.034" style="enable-background:new 0 0 551.034 551.034;" xml:space="preserve">
                                    <g>
                                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="275.517" y1="4.57" x2="275.517" y2="549.72" gradientTransform="matrix(1 0 0 -1 0 554)">
                                            <stop offset="0" style="stop-color:#E09B3D"/>
                                            <stop offset="0.3" style="stop-color:#C74C4D"/>
                                            <stop offset="0.6" style="stop-color:#C21975"/>
                                            <stop offset="1" style="stop-color:#7024C4"/>
                                        </linearGradient>
                                        <path style="fill:url(#SVGID_1_);" d="M386.878,0H164.156C73.64,0,0,73.64,0,164.156v222.722   c0,90.516,73.64,164.156,164.156,164.156h222.722c90.516,0,164.156-73.64,164.156-164.156V164.156   C551.033,73.64,477.393,0,386.878,0z M495.6,386.878c0,60.045-48.677,108.722-108.722,108.722H164.156   c-60.045,0-108.722-48.677-108.722-108.722V164.156c0-60.046,48.677-108.722,108.722-108.722h222.722   c60.045,0,108.722,48.676,108.722,108.722L495.6,386.878L495.6,386.878z"/>
                                        
                                            <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="275.517" y1="4.57" x2="275.517" y2="549.72" gradientTransform="matrix(1 0 0 -1 0 554)">
                                            <stop offset="0" style="stop-color:#E09B3D"/>
                                            <stop offset="0.3" style="stop-color:#C74C4D"/>
                                            <stop offset="0.6" style="stop-color:#C21975"/>
                                            <stop offset="1" style="stop-color:#7024C4"/>
                                        </linearGradient>
                                        <path style="fill:url(#SVGID_2_);" d="M275.517,133C196.933,133,133,196.933,133,275.516s63.933,142.517,142.517,142.517   S418.034,354.1,418.034,275.516S354.101,133,275.517,133z M275.517,362.6c-48.095,0-87.083-38.988-87.083-87.083   s38.989-87.083,87.083-87.083c48.095,0,87.083,38.988,87.083,87.083C362.6,323.611,323.611,362.6,275.517,362.6z"/>
                                        
                                            <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="418.31" y1="4.57" x2="418.31" y2="549.72" gradientTransform="matrix(1 0 0 -1 0 554)">
                                            <stop offset="0" style="stop-color:#E09B3D"/>
                                            <stop offset="0.3" style="stop-color:#C74C4D"/>
                                            <stop offset="0.6" style="stop-color:#C21975"/>
                                            <stop offset="1" style="stop-color:#7024C4"/>
                                        </linearGradient>
                                        <circle style="fill:url(#SVGID_3_);" cx="418.31" cy="134.07" r="34.15"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                </div>         
        </header> 
    </body>
</html>