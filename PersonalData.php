<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="PData.css">
        <title>Personal Data</title>
    </head>
    <body>
        <header>
            <div class="title">
                <h1>Personal Data</h1>
            </div>
                <div class="data">
                    <table>
                        <tr>
                            <td>Birth Date:</td>
                            <td>January 09, 1998</td>
                        </tr>
                        <tr>
                            <td>Birth Place:</td>
                            <td>Davao City</td>
                        </tr>
                        <tr>
                            <td>Age:</td>
                            <td>20 yrs. old</td>
                        </tr>
                        <tr>
                            <td>Sex:</td>
                            <td>Male</td>
                        </tr>
                        <tr>
                            <td>Citizenship:</td>
                            <td>Filipino</td>
                        </tr>
                        <tr>
                            <td>Religion:</td>
                            <td>Roman Catholic</td>
                        </tr>
                        <tr>
                            <td>Height:</td>
                            <td>5'7"</td>
                        </tr>
                        <tr>
                            <td>Civil Status:</td>
                            <td>Single</td>
                        </tr>
            
                        <tr>
                            <td>Language:</td>
                            <td>Filipino & English</td>
                        </tr>         
                    </table>
                </div>
        </header>
    </body>
</html>